const { square } = require("./math");

describe("square function", () => {

  test("can square two", () => {
    const mockMultiple = (n1, n2) => {
      expect(n1).toBe(2);
      expect(n2).toBe(2);
      return 4;
    }

    const result = square(2, mockMultiple);
    expect(result).toBe(4);
  });

  // edge case
  test("can square zero", () => {
    const result = square(0);
    expect(result).toBe(0);
  })
});
