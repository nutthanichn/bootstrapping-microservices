function square(n, multiply) {
  return multiply(n, n);
}

module.exports = {
  square,
};
